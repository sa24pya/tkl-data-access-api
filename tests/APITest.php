<?php
/**
 * File: tests/APITest.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataAccessAPI
 * @subpackage API
 * @version 1.0.6
 *
 */

namespace DataAccessAPI;

use DataAccessDAL\DAL;
use DataAccessACL\ACL;
use Dotenv\Dotenv;

/**
 * Class APITest
 *
 * @package DataAccessAPI
 * @subpackage API
 * @version 1.0.6
 */
class _APITest extends \PHPUnit_Framework_TestCase
{


    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    /**
     * Server URL
     * @var string
     */
    //  todo: remove const SERVER_URL = "http://m2.manager.shobr.com" ;

    public function setUp()
    {
        // $this->populatePersonDB();
    }
    /*
    public function tearDown()
    {
        $mongoServer = 'localhost';
        $mongoPort = 27017;
        $manager = new \MongoDB\Driver\Manager("mongodb://$mongoServer:$mongoPort");
        $command = new \MongoDB\Driver\Command([
            'drop' => 'person',
        ]);
        $manager->executeCommand('SA24', $command);
    }
    
    private function populatePersonDB()
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com',
            'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
            'ACLRole'=>'Supplier_Editor',
            'glns'=> ['5790000010974']
        ];
        $dal = DAL::getInstance();
        $dal->personSave((object)$person);
        $person =[
            'client'=>'shobrManager',
            'email'=> rand(0, 5000) . '@' . rand(0, 5000) . '.org',
            'password'=> '123',
            'ACLRole'=> 'Supplier_Editor',
            'glns'=> '11110000010974'
        ];
        $dal = DAL::getInstance();
        return $dal->personSave((object)$person);
    }
    */

    public function testHeaderCommented()
    {
        echo "-----------------------------------------\n";
        echo " WARNING!!! \n The TEST will FAIL \n"
            . " if the header() lines are not commented \n"
            . " in API::sendResponse() \n" ;
        echo "------------------------------------------\n";
    }

    /**
     * Test person authenticate
     *
     * The line 204 in API must be commented in order to work
     */
    /*
    public function testPersonAuthenticate()
    {
        $credentials = ['email'=>'cma@abena.dk', 'password'=>'Abena'];
        $api = new \DataAccessAPI\API();
        ob_start();
        $api->personAuthenticate($credentials);
        $result = ob_get_flush();
        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));
        $this->tokenTest(json_decode(json_encode($result['token']), true));
    }
    */
    /**
     * Test person authenticate
     *
     * The line 204 in API must be commented in order to work
     */

/*
    public function testPersonRead()
    {
        $credentials = ['email'=>'cms@abena.com', 'password'=>'Abena'];
        $api = new \DataAccessAPI\API();

        ob_start();
        $api->personAuthenticate($credentials);
        $response = ob_get_flush();

        $response = json_decode($response);
        $response = json_decode(json_encode($response), true);
        $request['token'] = json_encode($response['token']);
        $request['filters'] = json_encode($credentials) ;

        ob_start();
        $api->personRead($request);
        $result = ob_get_flush();

        $result = (array) json_decode($result);

        // var_dump($result);

        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));
        $this->tokenTest(json_decode($result['token'], true));
        $this->assertTrue(array_key_exists('queryResult', $result));
        $this->assertTrue(!empty($result['queryResult']));
        $aPerson = (array)$result['queryResult'][0];
        $this->assertTrue(array_key_exists('email', $aPerson));
        $this->assertTrue(!empty($aPerson['email']));
        $this->assertTrue($credentials['email'] == $aPerson['email']);

    }
    */


    public function testCatalogueItem()
    {
        $credentials = ['email'=>'cma@abena.dk', 'password'=>'Abena'];
        $api = new \DataAccessAPI\API();
        ob_start();
        $api->personAuthenticate($credentials);
        $result = ob_get_flush();
        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));

        $token = $result['token'];
        $this->tokenTest(json_decode(json_encode($token), true));

        /*
        // save catalogue Item
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790000010974',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '424242',
            ]
        ];

        ob_start();
        $api->catalogueItemSave(['token' => json_encode($token), 'document' => json_encode($catalogueOriginal)]);
        $result = ob_get_flush();

        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));

        */
        ob_start();
        $api->catalogueItemRead([
            'token' => json_encode($token),
            'filters'=> json_encode(['catalogueItem.tradeItem.informationProviderOfTradeItem.gln' => '5790000000371'])
        ]);
        $result = ob_get_flush();

        $result = (array) json_decode($result);
        $result = json_decode(json_encode($result), true);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));
        $tokenRead = $result['token'];
        $this->tokenTest($tokenRead);
        unset($result['queryResult']['SA24']->_id);

        // should be in ['queryResult']['SA24'][0] but is in 1
        $this->assertTrue(!empty($result['queryResult']['SA24'][0]));
    }


/*
    public function testCatalogueItemPush()
    {
        $credentials = ['email'=>'eko@shopall24.com', 'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'];
        $api = new \DataAccessAPI\API();
        ob_start();
        $api->personAuthenticate($credentials);
        $result = ob_get_flush();
        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));


        $token = $result['token'];
        // save catalogue Item
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790000010974',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '424242',
            ]
        ];

        ob_start();
        $api->catalogueItemPush(['token' => json_encode( $token ), 'document' => json_encode($catalogueOriginal)]);
        $result = ob_get_flush();

        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue( array_key_exists('token', $result));
        $this->assertTrue( !empty($result['token']));

        $token = $result['token'];
        ob_start();
        $api->catalogueItemRead([ 'token' => json_encode($token), 'filters'=> json_encode(['tradeItem.gtin' => '424242']) ]);
        $result = ob_get_flush();
        $result = (array) json_decode($result);
        $result = json_decode(json_encode($result),true);
        $this->assertTrue(is_array($result));
        $this->assertTrue( array_key_exists('token', $result));
        $this->assertTrue( !empty($result['token']));
        unset( $result['queryResult']['SA24']->_id);

        $this->assertTrue(
            isset($result['queryResult']['SA24'][0]["push"]["pending"]) && $result['queryResult']['SA24'][0]["push"]["pending"] == true
        );
    }

    public function testStockRead()
    {

        //$dotenv = new Dotenv(dirname(__DIR__));
        //$dotenv->load();

        $credentials = ['email'=>'eko@shopall24.com', 'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'];
        $api = new \DataAccessAPI\API();
        ob_start();
        $api->personAuthenticate($credentials);
        $result = ob_get_flush();
        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));


        $token = $result['token'];

        $this->tokenTest(json_decode(json_encode($token), true));


        ob_start();
        $api->stockRead(
            [
                'token' => json_encode($token),
                'filters'=> ['glns' => '5790000010974']
            ]
        );
        $result = ob_get_flush();

        $result = (array) json_decode($result);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('token', $result));

        $this->assertTrue(!empty($result['token']));

        $token = $result['token'];

        $this->assertTrue(!empty($token));
        $this->tokenTest(json_decode($token, true));
    }

    public function testTokenDelete()
    {

        $credentials = ['email'=>'eko@shopall24.com', 'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'];
        $api = new \DataAccessAPI\API();

        ob_start();
        $api->personAuthenticate($credentials);
        $response = ob_get_flush();

        $response = json_decode($response);
        $response = json_decode(json_encode($response), true);
        $request['token'] = json_encode($response['token']);
        $request['filters'] = $response['token'];
        ob_start();
        $api->tokenDelete($request);
        $result = ob_get_flush();

        $result = (array)json_decode($result);

        var_dump($result);
        $this->assertTrue($result["queryResult"]);
    }
    /**
     * tokenTest()
     * @param type $token : The token to be checked. IMPORTANT: It has to be an associative array.
     */
    
    private function tokenTest($token)
    {
        $this->assertTrue(isset($token['code']));
        $this->assertTrue(isset($token['expire']));
        $date = new \DateTime($token['expire']);
        $this->assertTrue($date > new \DateTime());
    }
    /**/
}
