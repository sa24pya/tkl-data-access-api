# TKL DataAccessAPI
TKL DataAccessAPI  
TKL DataAccessAPI provides Access Public Interface for data-access module.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-access-api
```

## Usage

``` bash
$ 
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](https://github.com/tkl/data-access-api/blob/master/CONTRIBUTING.md) for details.

## Credits

- [Robert Polsen](https://github.com/robertpolsen)
- [All Contributors](https://github.com/tkl/data-access-api/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
