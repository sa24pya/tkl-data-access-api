<?php
/**
 * File: API.php
 *
 * While I am opposed to all orthodox creeds, I have a creed myself;
 * and my creed is this.
 * Happiness is the only good.
 * The time to be happy is now.
 * The place to be happy is here.
 * The way to be happy is to make others so.
 * This creed is somewhat short, but it is long enough for this life,
 * strong enough for this world. If there is another world,
 * when we get there we can make another creed. "
 * - Robert Green Ingersoll
 *
 * @author: mrx
 *
 * @package: DataAccessAPI
 * @subpackage API
 * @version: 1.0.0
 *
 */

namespace DataAccessAPI;

// Requires
//require_once('ACL.php');

// .env
use Dotenv\Dotenv;
// Log
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


/**
 * Class API
 * It exposes Data-Access services.
 * Re-route service’s requests to ACL component.
 *
 * @package: DataAccessAPI
 * @subpackage API
 * @version: 1.0.0
 */
class API
{
    #m2.manager.shobr.com

    /**
     * Implemented HTTP codes
     * @var array
     */
    const HTTP_CODES_DESCRIPTIONS = [
        '200' => 'OK',
        '400' => 'Bad Request',
        '401' => 'Unauthorized',
        '403' => 'Forbidden',
        '404' => 'Not Found',
        '418' => 'I\'m a teapot',
        '500' => 'Server Error'
    ];


    /**
     * Available services & related required fileds
     * @var array $services
     */
    const SERVICE_LIST = [
        'personAuthenticate' => ['email','password'],
        'serviceList' => ['token'],
        'personRead' => ['token','filters'],
        'tokenDelete' => ['token','filters'],
        'catalogueItemRead' => ['token','filters'],
        'catalogueItemSave' => ['token', 'document'],
        'catalogueItemBulk' => ['token', 'documents'],

        'catalogueItemDelete' => ['token','filters'],
        'catalogueItemImport' => ['token','csvfile'],
        'catalogueItemPush' => ['token','document'],
        'stockRead' => ['token','filters'],
        'orderRead' => ['token','filters'],
        'orderWrite' => ['token','filters', 'document'],

    ];

    /**
     * logger (should be monolog).
     * @var Object
     */
    private $log;

    /**
     * Server response.
     * @var array
     */
    private $response = [];


    /**
     * Debug messages
     * @var array
     */
    private $responseMessages = [];


    /**
     * service name
     * @var string
     */
    private $servicename;

    /**
     * ACL object
     * @var object
     */
    public $acl ;

    /**
     * person that made the request
     * @var object stdObject
     */
    private $person;

    /**
     * Last generated token
     * @var array
     */
    private $tokenGenerated;



    /**
     * Magic call to ACL, the method that was called is
     * the name of the service or "serviceName"
     *
     * @param unknown $serviceName
     * @param unknown $arguments
     */
    public function __call($serviceName, $args)
    {
        // API conf: 
        $dotenv = new \Dotenv\Dotenv( dirname( __DIR__ ));
        $dotenv->load();

        // API log
        $this->log = new Logger('data-access-api');
        $this->log->pushHandler(new StreamHandler(getenv('PATH_TO_LOG_FILE'), Logger::DEBUG));
        $this->log->info("API call: $serviceName");
        $this->log->debug("Args: " . var_export($args, true));

        // Init response
        $this->initResponse();

        // Check if service exists. If not, set repsonse "400 bad request"
        $serviceExists = $this->serviceExists($serviceName);
        if (!$serviceExists) {
            return;
        }
        // service name
        $this->servicename = $serviceName;

        // Required Parameters validation. If failed set
        // repsonse "400 bad request"
        $parametersValidation = $this->parametersValidation($serviceName, $args[0]);
        if (!$parametersValidation) {
            return;
        }

        // ACL
        $this->acl = new \DataAccessACL\ACL();
        $this->acl->api = $this;
        // Service call on ACL
        $result = null ;

        try {
            // PreDispatch
            $this->preDispatch($serviceName, $args[0]);

            // Service call on ACL
            switch ($serviceName) {
                case 'personAuthenticate':
                    $credentials = $args[0];
                    unset($credentials['serviceName']);
                    $result = $this->acl->$serviceName($credentials);
                    break;
                case 'catalogueItemRead':
                    // Service call on ACL
                    $result = $this->acl->$serviceName(array(
                        'filters' => (array) json_decode($args[0]['filters'])));
                    break;
                case 'personRead':
                    break;
                case 'tokenDelete':
                    // Service call on ACL
                    $status = $this->acl->$serviceName($args[0]);
                    if(1 == $status->getDeletedCount()){
                        $result = true;
                    }
                    break;
                case 'catalogueItemDelete':
                    // Service call on ACL
                    $result = $this->acl->$serviceName(array(
                        'filters' => (array) json_decode($args[0]['filters'])));
                    break;
                case 'catalogueItemSave':
                    // Service call on ACL
                    $result = $this->acl->$serviceName(array(
                        'document' => json_decode($args[0]['document'])));
                    break;
                case 'catalogueItemPush':
                    // Service call on ACL
                    $result = $this->acl->$serviceName(array(
                        'document' => json_decode($args[0]['document'])));
                    break;
                default:
                    // Service call on ACL
                    $result = $this->acl->$serviceName($args[0]);
            }

            // result evaluation
            if (is_null($result)) {
                throw new \Exception("ACL null result.", 500);
            }
            if (false === $result) {
                throw new \Exception("Bad credentials (email&password)", 401);
            }

            // PostDispatch: modfify $result before sendResponse()
            $this->postDispatch($serviceName, $result);

            // HTTP code OK
            if (418 == $this->response['status']) {
                $this->setResponseStatus('200');
            }

            // Send Response
            $this->sendResponse();
        } catch (\Exception $e) {
            // Errors 500 and so
            $this->setResponseStatus('500');
            if ($code = $e->getCode()) {
                $this->setResponseStatus($code);
            }
            $this->responseMessagePush("Servicename: " . $serviceName);
            $this->responseMessagePush($e->getMessage());
            if (isset($args[0]['token'])) {
                $this->tokenGenerated = $args[0]['token'] ;
            }
            
            //var_dump($e->getMessage());
            //var_dump($e->getFile());
            //var_dump($e->getLine());exit;

            $this->sendResponse();
            return ;
        }
    }

    /**
     * Check if service exists. If not set repsonse "404 not found"
     * @param string $serviceName
     */
    private function serviceExists($serviceName)
    {
        if (!array_key_exists($serviceName, self::SERVICE_LIST)) {
            // Missing service name
            $this->setResponseStatus('404');
            $message = "Invalid serviceName: $serviceName";
            $this->responseMessagePush($message);
            $this->sendResponse();
            return false ;
        }
        return true ;
    }

    /**
     * Parameters validation.
     * If failed, set a response "400 bad request"
     */
    private function parametersValidation($serviceName, $args)
    {
        foreach (self::SERVICE_LIST[$serviceName] as $i => $requiredField) {
            if (!array_key_exists($requiredField, $args)) {
                // Missing parameter
                $this->setResponseStatus('400');
                $message = "missing parameter: '$requiredField'";
                $this->responseMessagePush($message);
                $this->sendResponse();
                return false ;
            }
        }
        return true ;
    }


    /**
     * it process the Request.
     */
    private function preDispatch($serviceName, $request)
    {
        if ('personAuthenticate' != $serviceName) {
            // Token check
            $this->tokenValidate($request);
            // $this->regenerateToken($request);
        }
    }


    /**
     * Valiate Token
     *
     * @param array $request It MUST contains "token" index
     * @throws Exception when token expires
     */
    private function tokenValidate($request)
    {
        unset($request['serviceName']);
        if (!array_key_exists('token', $request)) {
            $message = 'No token provided';
            throw new \Exception($mesasge, 401);
        }

        //file_put_contents( 'token.txt' , var_export($token,true));

        $filters['filters'] = (array)json_decode($request['token']);
        $token = $this->acl->tokenRead($filters);
        if(!$token) {
            $message = "Token not valid." ;
            throw new \Exception($message, 401);
        }

        //file_put_contents( 'validateTokenResult.txt' , var_export($result,true));

        $expire = $token->expire;

        $d1 = new \DateTime($expire, new \DateTimeZone('Europe/Copenhagen'));
        $d2 = new \DateTime('now', new \DateTimeZone('Europe/Copenhagen'));

        if ($d1 <= $d2) {
            $message = 'token expired';
            throw new \Exception($message, 401);
        }
        unset($token->_id);
        $this->tokenGenerated = $token;
        $person = [
            '_id' => $token->person_id
        ];
        $person = $this->acl->personRead(["filters"=>$person]);
        $this->acl->setPerson((object) $person[0]);
    }

    private function tokenRegenerate($request)
    {
        $token = $this->acl->tokenCreate();
        $this->tokenGenerated = $token ;
    }


    /**
     * It process the response.
     *
     * @param unknown $serviceName
     * @param unknown $result
     */
    private function postDispatch($serviceName, $result)
    {
        switch ($serviceName) {
            case 'personAuthenticate':
                if (!$result) {
                    $this->setResponseStatus(401);
                } else {
                    $this->setResponseStatus(200);
                    $token = $result['token'];
                    $this->setResponseToken($token);
                }
                break;
            case 'tokenDelete' :
                $this->setResponseResult($result);
                $this->setResponseToken(new \stdClass());
                break;
            default:
                //$this->setResponseStatus(200);
                $this->setResponseResult($result);
                $this->setResponseToken($this->tokenGenerated);
        }
        return $result;
    }

    /**
     * Set response status
     * @param string $status
     */
    private function setResponseStatus($status)
    {
        $this->response['status'] = $status ;
    }

    /**
     * Set response $statusDescription
     * @param string $status
     */
    private function setResponseStatusDescription($statusDescription)
    {
        $this->response['statusDescription'] = $statusDescription ;
    }

    /**
     * Set response $token
     * @param string $token
     */
    private function setResponseToken($token)
    {
        $this->response['token'] = $token ;
    }

    /**
     * Add result to response
     */
    private function setResponseResult($result)
    {
        $this->response['queryResult'] = $result;
    }

    /**
     * Push a message to the collection of messages that
     * will be sent to the client when self::sendResponse().
     *
     * @param string $message
     */
    private function responseMessagePush($message)
    {
        array_push($this->responseMessages, $message);
    }


    private function initResponse()
    {
        $response = [
            'status' => 418,
            'statusDescription' => 'I\'m a teapot',
            'token' => null,
        ];
        $this->response = $response;
    }



    /**
     * Send HTTP response
     * header & body to client.
     *
     */
    private function sendResponse()
    {
        // HTTP header
        $status = (string) $this->response['status'];
        $this->setResponseStatusDescription(self::HTTP_CODES_DESCRIPTIONS[$status]);
        $statusDescription = $this->response['statusDescription'];

        // When PHPUnit comment following line
        // X-domain requirement
        // header("Access-Control-Allow-Origin: *");

        // HTTP Headers:
        // Status code
        // When PHPUnit comment following line
        // header("HTTP/1.0 $status $statusDescription");

    
        // HTTP body
        $response = $this->response;

        // Additional (not funcional, more debug) messages
        $response['messages'] = $this->responseMessages ;

        // Encoding JSON
        $jresponse = json_encode($response);
        $this->log->debug("API response: " . $jresponse);

        //file_put_contents( "resultj.txt" , var_export($response,true));

        // Send to client
        // header('Content-Type: application/json');
        print $jresponse;
    }




    /**
     * Service list
     * Also, how-to implement a specific "not magic" service method
     */
    public function serviceList($args)
    {
        // ServiceName
        $serviceName = 'serviceList';

        // Init response
        $this->initResponse();

        // Check if service exists. If not, set repsonse "400 bad request"
        $serviceExists = $this->serviceExists('serviceList');
        if (!$serviceExists) {
            return;
        }
        // service name
        $this->servicename = $serviceName;

        // Required Parameters validation. If failed set
        // repsonse "400 bad request"
        $parametersValidation = $this->parametersValidation($serviceName, $args);
        if (!$parametersValidation) {
            return;
        }

        // ACL
        $this->acl = new \DataAccessACL\ACL();
        // Service call on ACL
        $result = null ;

        try {
            // PreDispatch
            $this->preDispatch($serviceName, $args);

            // result
            $result = self::SERVICE_LIST;

            // PostDispatch: modfify $result before sendResponse()
            $this->postDispatch($serviceName, $result);

            if (418 == $this->response['status']) {
                $this->setResponseStatus('200');
            }

            // Send Response
            $this->sendResponse();
        } catch (\Exception $e) {
            // Errors 500 and so
            $this->setResponseStatus('500');
            if ($code = $e->getCode()) {
                $this->setResponseStatus($code);
            }
            $this->responseMessagePush("Servicename: " . 'serviceList');
            $this->responseMessagePush($e->getMessage());
            $this->sendResponse();
            return ;
        }
    }
}
